export const ACTION_PENDING = 'pending';
export const ACTION_RESOLVING = 'resolving';
export const ACTION_RESOLVED = 'resolved';
export const ACTION_ERROR = 'errored';
export const ACTION_ERROR_FALLBACK = 'fallback';
export const ACTION_PRELOAD = 'preload';

export const ANALYTICS_RESOLVING = 'resolving';
export const ANALYTICS_ERROR = 'errored';
export const ANALYTICS_FALLBACK = 'fallback';

export const ERROR_MESSAGE_OAUTH = 'Provider.authFlow is not set to OAuth2.';
export const ERROR_MESSAGE_FATAL = 'Fatal error resolving URL';
