import { css } from '@emotion/core';

export const HoverCardContainer = css`
  background: none;
  border-width: 0;
  max-width: 500px;
  padding: 0;
`;
