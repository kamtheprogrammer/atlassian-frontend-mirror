import { SmartLinkSize } from '../../../../constants';

export type ElementProps = {
  size?: SmartLinkSize;
  testId?: string;
};
