import { BlockProps } from '../types';

export type SnippetBlockProps = BlockProps & {
  maxLines?: number;
};
