import { defineMessages } from 'react-intl-next';

export const messages = defineMessages({
  giveFeedback: {
    defaultMessage: 'Give feedback',
    id: 'proforma-form-builder.give-feedback',
    description: 'The button that allows users to give feedback',
  },
  feedbackIconLabel: {
    defaultMessage: 'Feedback',
    id: 'proforma-form-builder.feedback-icon-label',
    description: 'Accessibility text used for the feedback icon',
  },
});
