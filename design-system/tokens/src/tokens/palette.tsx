import type { PaletteColorTokenSchema } from '../types';

const palette: PaletteColorTokenSchema = {
  color: {
    palette: {
      B100: {
        value: '#E9F2FF',
        attributes: { group: 'palette' },
      },
      B200: {
        value: '#CCE0FF',
        attributes: { group: 'palette' },
      },
      B300: {
        value: '#85B8FF',
        attributes: { group: 'palette' },
      },
      B400: {
        value: '#579DFF',
        attributes: { group: 'palette' },
      },
      B500: {
        value: '#388BFF',
        attributes: { group: 'palette' },
      },
      B600: {
        value: '#1D7AFC',
        attributes: { group: 'palette' },
      },
      B700: {
        value: '#0C66E4',
        attributes: { group: 'palette' },
      },
      B800: {
        value: '#0055CC',
        attributes: { group: 'palette' },
      },
      B900: {
        value: '#09326C',
        attributes: { group: 'palette' },
      },
      B1000: {
        value: '#082145',
        attributes: { group: 'palette' },
      },
      R100: {
        value: '#FFEDEB',
        attributes: { group: 'palette' },
      },
      R200: {
        value: '#FFD2CC',
        attributes: { group: 'palette' },
      },
      R300: {
        value: '#FF9C8F',
        attributes: { group: 'palette' },
      },
      R400: {
        value: '#F87462',
        attributes: { group: 'palette' },
      },
      R500: {
        value: '#EF5C48',
        attributes: { group: 'palette' },
      },
      R600: {
        value: '#E34935',
        attributes: { group: 'palette' },
      },
      R700: {
        value: '#CA3521',
        attributes: { group: 'palette' },
      },
      R800: {
        value: '#AE2A19',
        attributes: { group: 'palette' },
      },
      R900: {
        value: '#601E16',
        attributes: { group: 'palette' },
      },
      R1000: {
        value: '#391813',
        attributes: { group: 'palette' },
      },
      Y100: {
        value: '#FFF7D6',
        attributes: { group: 'palette' },
      },
      Y200: {
        value: '#F8E6A0',
        attributes: { group: 'palette' },
      },
      Y300: {
        value: '#F5CD47',
        attributes: { group: 'palette' },
      },
      Y400: {
        value: '#E2B203',
        attributes: { group: 'palette' },
      },
      Y500: {
        value: '#CF9F02',
        attributes: { group: 'palette' },
      },
      Y600: {
        value: '#B38600',
        attributes: { group: 'palette' },
      },
      Y700: {
        value: '#946F00',
        attributes: { group: 'palette' },
      },
      Y800: {
        value: '#7F5F01',
        attributes: { group: 'palette' },
      },
      Y900: {
        value: '#533F04',
        attributes: { group: 'palette' },
      },
      Y1000: {
        value: '#3D2E00',
        attributes: { group: 'palette' },
      },
      G100: {
        value: '#DFFCF0',
        attributes: { group: 'palette' },
      },
      G200: {
        value: '#BAF3DB',
        attributes: { group: 'palette' },
      },
      G300: {
        value: '#7EE2B8',
        attributes: { group: 'palette' },
      },
      G400: {
        value: '#4BCE97',
        attributes: { group: 'palette' },
      },
      G500: {
        value: '#2ABB7F',
        attributes: { group: 'palette' },
      },
      G600: {
        value: '#22A06B',
        attributes: { group: 'palette' },
      },
      G700: {
        value: '#1F845A',
        attributes: { group: 'palette' },
      },
      G800: {
        value: '#216E4E',
        attributes: { group: 'palette' },
      },
      G900: {
        value: '#164B35',
        attributes: { group: 'palette' },
      },
      G1000: {
        value: '#133527',
        attributes: { group: 'palette' },
      },
      P100: {
        value: '#F3F0FF',
        attributes: { group: 'palette' },
      },
      P200: {
        value: '#DFD8FD',
        attributes: { group: 'palette' },
      },
      P300: {
        value: '#B8ACF6',
        attributes: { group: 'palette' },
      },
      P400: {
        value: '#9F8FEF',
        attributes: { group: 'palette' },
      },
      P500: {
        value: '#8F7EE7',
        attributes: { group: 'palette' },
      },
      P600: {
        value: '#8270DB',
        attributes: { group: 'palette' },
      },
      P700: {
        value: '#6E5DC6',
        attributes: { group: 'palette' },
      },
      P800: {
        value: '#5E4DB2',
        attributes: { group: 'palette' },
      },
      P900: {
        value: '#352C63',
        attributes: { group: 'palette' },
      },
      P1000: {
        value: '#231C3F',
        attributes: { group: 'palette' },
      },
      T100: {
        value: '#E3FAFC',
        attributes: { group: 'palette' },
      },
      T200: {
        value: '#C1F0F5',
        attributes: { group: 'palette' },
      },
      T300: {
        value: '#8BDBE5',
        attributes: { group: 'palette' },
      },
      T400: {
        value: '#60C6D2',
        attributes: { group: 'palette' },
      },
      T500: {
        value: '#37B4C3',
        attributes: { group: 'palette' },
      },
      T600: {
        value: '#1D9AAA',
        attributes: { group: 'palette' },
      },
      T700: {
        value: '#1D7F8C',
        attributes: { group: 'palette' },
      },
      T800: {
        value: '#206B74',
        attributes: { group: 'palette' },
      },
      T900: {
        value: '#1D474C',
        attributes: { group: 'palette' },
      },
      T1000: {
        value: '#153337',
        attributes: { group: 'palette' },
      },
      O100: {
        value: '#FFF4E5',
        attributes: { group: 'palette' },
      },
      O200: {
        value: '#FFE2BD',
        attributes: { group: 'palette' },
      },
      O300: {
        value: '#FEC57B',
        attributes: { group: 'palette' },
      },
      O400: {
        value: '#FAA53D',
        attributes: { group: 'palette' },
      },
      O500: {
        value: '#F18D13',
        attributes: { group: 'palette' },
      },
      O600: {
        value: '#D97008',
        attributes: { group: 'palette' },
      },
      O700: {
        value: '#B65C02',
        attributes: { group: 'palette' },
      },
      O800: {
        value: '#974F0C',
        attributes: { group: 'palette' },
      },
      O900: {
        value: '#5F3811',
        attributes: { group: 'palette' },
      },
      O1000: {
        value: '#43290F',
        attributes: { group: 'palette' },
      },
      M100: {
        value: '#FFECF8',
        attributes: { group: 'palette' },
      },
      M200: {
        value: '#FDD0EC',
        attributes: { group: 'palette' },
      },
      M300: {
        value: '#F797D2',
        attributes: { group: 'palette' },
      },
      M400: {
        value: '#E774BB',
        attributes: { group: 'palette' },
      },
      M500: {
        value: '#DA62AC',
        attributes: { group: 'palette' },
      },
      M600: {
        value: '#CD519D',
        attributes: { group: 'palette' },
      },
      M700: {
        value: '#AE4787',
        attributes: { group: 'palette' },
      },
      M800: {
        value: '#943D73',
        attributes: { group: 'palette' },
      },
      M900: {
        value: '#50253F',
        attributes: { group: 'palette' },
      },
      M1000: {
        value: '#341829',
        attributes: { group: 'palette' },
      },
      'DN-100': {
        value: '#101214',
        attributes: { group: 'palette' },
      },
      'DN-100A': {
        // #030404 26%
        value: '#03040442',
        attributes: { group: 'palette' },
      },
      DN0: {
        value: '#161A1D',
        attributes: { group: 'palette' },
      },
      DN100: {
        value: '#1D2125',
        attributes: { group: 'palette' },
      },
      DN100A: {
        // #BCD6F0 4%
        value: '#BCD6F00A',
        attributes: { group: 'palette' },
      },
      DN200: {
        value: '#22272B',
        attributes: { group: 'palette' },
      },
      DN200A: {
        // #A1BDD9 8%
        value: '#A1BDD914',
        attributes: { group: 'palette' },
      },
      DN300: {
        value: '#2C333A',
        attributes: { group: 'palette' },
      },
      DN300A: {
        // #A6C5E2 16%
        value: '#A6C5E229',
        attributes: { group: 'palette' },
      },
      DN400: {
        value: '#454F59',
        attributes: { group: 'palette' },
      },
      DN400A: {
        // #BFDBF8 28%
        value: '#BFDBF847',
        attributes: { group: 'palette' },
      },
      DN500: {
        value: '#5C6C7A',
        attributes: { group: 'palette' },
      },
      DN500A: {
        // #A9C5DF 48%
        value: '#A9C5DF7A',
        attributes: { group: 'palette' },
      },
      DN600: {
        value: '#738496',
        attributes: { group: 'palette' },
      },
      DN700: {
        value: '#8696A7',
        attributes: { group: 'palette' },
      },
      DN800: {
        value: '#9FADBC',
        attributes: { group: 'palette' },
      },
      DN900: {
        value: '#B6C2CF',
        attributes: { group: 'palette' },
      },
      DN1000: {
        value: '#C7D1DB',
        attributes: { group: 'palette' },
      },
      DN1100: {
        value: '#DEE4EA',
        attributes: { group: 'palette' },
      },
      N0: { value: '#FFFFFF', attributes: { group: 'palette' } },
      N100: {
        value: '#F7F8F9',
        attributes: { group: 'palette' },
      },
      N100A: {
        // #091E42 3%
        value: '#091E4208',
        attributes: { group: 'palette' },
      },
      N200: {
        value: '#F1F2F4',
        attributes: { group: 'palette' },
      },
      N200A: {
        // #091E42 6%
        value: '#091E420F',
        attributes: { group: 'palette' },
      },
      N300: {
        value: '#DCDFE4',
        attributes: { group: 'palette' },
      },
      N300A: {
        // #091E42 14%
        value: '#091E4224',
        attributes: { group: 'palette' },
      },
      N400: {
        value: '#B3B9C4',
        attributes: { group: 'palette' },
      },
      N400A: {
        // #091E42 31%
        value: '#091E424F',
        attributes: { group: 'palette' },
      },
      N500: {
        value: '#8993A5',
        attributes: { group: 'palette' },
      },
      N500A: {
        // #091E42 48%
        value: '#091E427A',
        attributes: { group: 'palette' },
      },
      N600: {
        value: '#758195',
        attributes: { group: 'palette' },
      },
      N700: {
        value: '#626F86',
        attributes: { group: 'palette' },
      },
      N800: {
        value: '#44546F',
        attributes: { group: 'palette' },
      },
      N900: {
        value: '#2C3E5D',
        attributes: { group: 'palette' },
      },
      N1000: {
        value: '#172B4D',
        attributes: { group: 'palette' },
      },
      N1100: {
        value: '#091E42',
        attributes: { group: 'palette' },
      },
    },
  },
};

export default palette;
