import type { DrawerWidth } from './components/types';

export const transitionDuration = '0.22s';
export const transitionDurationMs = 220;
export const transitionTimingFunction = 'cubic-bezier(0.2, 0, 0, 1)';
export const widths: DrawerWidth[] = [
  'narrow',
  'medium',
  'wide',
  'extended',
  'full',
];
